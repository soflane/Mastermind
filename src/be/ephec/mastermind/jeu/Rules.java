package be.ephec.mastermind.jeu;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Font;

/**
 * JPanel contenant les r�gles du jeu et un EasterEgg.
 * 
 * @author R�my Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Rules extends JPanel implements ActionListener{
	/**
	 * JLabel contenant le texte des r�gles du jeu.
	 */
	private JLabel rulesLbl;
	/**
	 * Bouton permettant de retourner au menu.
	 */
	private Button btnMenu;
	/**
	 * Fen�tre principale de l'application.
	 */
	private MainFrame mf;
	/**
	 * Cr�ation du JPanel.
	 * @param mf Fen�tre principale de l'application.
	 */
	public Rules(MainFrame mf) {
		this.mf = mf;
		this.setLayout(null);
		
		rulesLbl = new JLabel(Param.RULES);
		rulesLbl.setFont(new Font("Tahoma", Font.BOLD, 20));
		rulesLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		rulesLbl.setHorizontalAlignment(SwingConstants.CENTER);
		rulesLbl.setBounds(75, 25, Param.FRAMEWIDTH-150, Param.FRAMEHEIGHT-200);
		rulesLbl.setForeground(Color.WHITE);
		this.add(rulesLbl);
		
		btnMenu = new Button("menu");
		btnMenu.setBounds((Param.FRAMEWIDTH/2)-(193/2), Param.FRAMEHEIGHT-100, 193, 54);
		btnMenu.addActionListener(this);
		this.add(btnMenu);
		
		Background bg = new Background();
		this.add(bg);
	}
	/**
	 * Actions au clic du bouton.
	 */
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "menu":
			mf.menu();
			Param.EASTEREGG++;
			if(Param.EASTEREGG==5){
				AudioClip music = Applet.newAudioClip(getClass().getResource("/musique.wav"));
				music.play();
				JOptionPane.showMessageDialog(null, "", "Enjoy <3", 1, new ImageIcon(getClass().getResource("/gandalf.gif")));
				music.stop();
				Param.EASTEREGG = 0;
			}
			break;

		default:
			break;
		}
	}

}

package be.ephec.mastermind.jeu;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Cursor;

/**
 * JPanel du mode solitaire.
 * 
 * @author R�my Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 */
@SuppressWarnings({ "serial" })
public class Solo extends JPanel implements ActionListener{
	/**
	 * Fen�tre principale de l'application.
	 */
	private MainFrame mf;
	/**
	 * Label contenant les r�gles du jeu.
	 */
	private JLabel rulesLbl;
	/**
	 * plateau de jeu.
	 */
	private Board b;
	/**
	 * JLabel contenant les erreurs du jeu.
	 */
	private JLabel error;
	/**
	 * Bouton permettant d'enlever le son.
	 */
	private JLabel btnMute;
	/**
	 * Icone du bouton mute.
	 */
	private ImageIcon muteIcon = new ImageIcon(getClass().getResource("/mute.png"));
	/**
	 * Bouton pour retourner au menu.
	 */
	private Button btnMenu;
	/**
	 * Bouton pour afficher les r�gles.
	 */
	private Button btnRules;
	/**
	 * Cr�ation du JPanel Solitaire.
	 * @param mf Fen�tre principale de l'application.
	 */
	public Solo(MainFrame mf) {
		this.setLayout(null);
		this.mf = mf;
		
		b = new Board(mf);
		b.setSolo(this);
		b.setBounds((Param.FRAMEWIDTH/2)-(350/2), 0, 350, Param.FRAMEHEIGHT);
		this.add(b);
		
		btnMenu = new Button("menu");
		btnMenu.setBounds(Param.FRAMEWIDTH-250, 50, 193, 54);
		btnMenu.addActionListener(this);
		this.add(btnMenu);
		
		rulesLbl = new JLabel(Param.RULES);
		rulesLbl.setFont(new Font("Tahoma", Font.BOLD, 12));
		rulesLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		rulesLbl.setHorizontalAlignment(SwingConstants.CENTER);
		rulesLbl.setBounds(55, 75, Param.FRAMEWIDTH/4, Param.FRAMEHEIGHT-200);
		rulesLbl.setForeground(Color.WHITE);
		rulesLbl.setVisible(false);
		this.add(rulesLbl);
		
		btnRules = new Button("rules1");
		btnRules.setBounds(65, 50, 193, 54);
		btnRules.addActionListener(this);
		this.add(btnRules);
		
		error = new JLabel("");
		error.setFont(new Font("Tahoma", Font.BOLD, 15));
		error.setForeground(Color.RED);
		error.setBounds(Param.FRAMEWIDTH-250, 150, 200, 20);
		this.add(error);
		
		btnMute = new JLabel();
		btnMute.setIcon(muteIcon);
		btnMute.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnMute.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				b.mute();
			}
		});
		btnMute.setBounds(5, 5, 32, 20);
		this.add(btnMute);
		
		Background bg = new Background();
		this.add(bg);
	}
	/**
	 * Getter du JLabel error.
	 * @return JLabel des erreurs.
	 */
	public JLabel getError() {
		return error;
	}
	/**
	 * Gestion des clics sur les diff�rents boutons.
	 */
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "menu":
			mf.menu();
			break;
		case "rules1" : 
			if(rulesLbl.isVisible()){
				rulesLbl.setVisible(false);
			}else{
				rulesLbl.setVisible(true);
			}
			break;
		default:
			break;
		}
	}
}

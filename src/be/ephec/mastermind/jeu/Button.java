package be.ephec.mastermind.jeu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 * Boutons étendant JButton pour le jeu.
 * 
 * @author Rémy Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Button extends JButton implements MouseListener{
	/**
	 * Nom du Bouton.
	 */
	private String name;
	/**
	 * Icone du bouton.
	 */
	private ImageIcon img;
	/**
	 * Icone du bouton quand la souris est dessus.
	 */
	private ImageIcon imgHover;
	/**
	 * Constructeur d'un bouton.
	 * @param name Nom du bouton.
	 */
	public Button(String name) {
		super(name);
		this.name=name;
		this.setHorizontalTextPosition(SwingConstants.CENTER);
		this.img = new ImageIcon(getClass().getResource("/"+this.name+".png"));
		this.imgHover = new ImageIcon(getClass().getResource("/"+this.name+"Hover.png"));
		this.setIcon(img);
		this.setBorder(null);
		this.addMouseListener(this);
		this.setBackground(new Color(0, 0, 0, 0));
		this.setForeground(new Color(0, 0, 0, 0));
		this.setOpaque(false);
		this.setFocusable(false);
		this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	}
	public String getName() {
		return name;
	}
	/**
	 * Non implémenté.
	 */
	public void mouseClicked(MouseEvent e) {
	}
	/**
	 * Change l'icone du bouton quand on passe la souris dessus.
	 */
	public void mouseEntered(MouseEvent e) {
		this.setIcon(imgHover);
	}
	/**
	 * Change l'icone du bouton quand on quitte la zone du bouton.
	 */
	public void mouseExited(MouseEvent e) {
		this.setIcon(img);
	}
	/**
	 * Non implémenté.
	 */
	public void mousePressed(MouseEvent e) {
	}
	/**
	 * Non implémenté.
	 */
	public void mouseReleased(MouseEvent e) {
	}
}
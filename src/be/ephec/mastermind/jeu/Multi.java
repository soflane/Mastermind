package be.ephec.mastermind.jeu;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import be.ephec.mastermind.reseau.ClientSocket;
import be.ephec.mastermind.reseau.ServeurSocket;

/**
 * JPanel du mode multijoueur.
 * 
 * @author R�my Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Multi extends JPanel implements ActionListener{
	/**
	 * Fen�tre principale de l'application.
	 */
	private MainFrame mf;
	/**
	 * Plateau de jeu.
	 */
	private Board b;
	/**
	 * Background du JPanel.
	 */
	private Background bg = new Background();
	/**
	 * Bouton permettant d'acceder au menu.
	 */
	private Button btnMenu;
	/**
	 * Socket du serveur.
	 */
	private ServeurSocket ss;
	/**
	 * Socket du client.
	 */
	private ClientSocket cs;
	/**
	 * Bouton permettant de passer son tour.
	 */
	private Button btnPass;
	/**
	 * Label permettant d'afficher les erreurs.
	 */
	private JLabel error;
	/**
	 * Label contenant les r�gles du jeu.
	 */
	private JLabel rulesLbl;
	/**
	 * Label contenant les informations sur le tour.
	 */
	private JLabel turnLbl;
	/**
	 * Bouton permettant d'afficher les r�gles.
	 */
	private Button btnRules;
	/**
	 * Bouton permettant d'enlever le son.
	 */
	private JLabel btnMute;
	/**
	 * Construction du JPanel Multijoueur en mode serveur.
	 * @param mf Fen�tre principale de l'application.
	 * @param ss Socket du serveur.
	 * @param b Plateau de jeu.
	 */
	public Multi(MainFrame mf, ServeurSocket ss, Board b) {
		this.b = b;
		createInterface(mf);
		this.ss = ss;
	}
	/**
	 * Construction du JPanel Multijoueur en mode client.
	 * @param mf Fen�tre principale de l'application.
	 * @param cs Socket du client.
	 * @param b Plateau de jeu.
	 */
	public Multi(MainFrame mf, ClientSocket cs, Board b) {
		this.b = b;
		createInterface(mf);
		this.cs = cs;
	}
	/**
	 * Cr�ation de l'interface utilisateur g�n�rale.
	 * @param mf fen�tre principale de l'application.
	 */
	public void createInterface(MainFrame mf) {
		this.setLayout(null);
		this.mf = mf;
		b.setBounds((Param.FRAMEWIDTH/2)-(350/2), 0, 350, Param.FRAMEHEIGHT);
		this.add(b);
		
		btnMenu = new Button("menu");
		btnMenu.setBounds(Param.FRAMEWIDTH-250, 50, 193, 54);
		btnMenu.addActionListener(this);
		this.add(btnMenu);
		
		btnPass = new Button("passeTour");
		btnPass.setBounds(Param.FRAMEWIDTH-250, 150, 193, 54);
		btnPass.addActionListener(this);
		this.add(btnPass);
		
		error = new JLabel("");
		error.setForeground(Color.RED);
		error.setFont(new Font("Tahoma", Font.BOLD, 15));
		error.setBounds(Param.FRAMEWIDTH-250, 300, 200, 20);
		this.add(error);
		
		rulesLbl = new JLabel(Param.RULES);
		rulesLbl.setFont(new Font("Tahoma", Font.BOLD, 12));
		rulesLbl.setHorizontalTextPosition(SwingConstants.CENTER);
		rulesLbl.setHorizontalAlignment(SwingConstants.CENTER);
		rulesLbl.setBounds(55, 75, Param.FRAMEWIDTH/4, Param.FRAMEHEIGHT-200);
		rulesLbl.setForeground(Color.WHITE);
		rulesLbl.setVisible(false);
		this.add(rulesLbl);
		
		btnRules = new Button("rules1");
		btnRules.setBounds(65, 50, 193, 54);
		btnRules.addActionListener(this);
		this.add(btnRules);
		
		turnLbl = new JLabel("");
		turnLbl.setBounds(Param.FRAMEWIDTH-250, 250, 200, 20);
		turnLbl.setFont(new Font("Tahoma", Font.BOLD, 15));
		turnLbl.setForeground(Color.WHITE);
		this.add(turnLbl);
		
		btnMute = new JLabel();
		btnMute.setIcon(new ImageIcon(getClass().getResource("/mute.png")));
		btnMute.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnMute.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e){
				b.mute();
			}
		});
		btnMute.setBounds(5, 5, 32, 20);
		this.add(btnMute);
		
		this.add(bg);
	}
	/**
	 * Getter du JLabel error.
	 * @return JLabel des erreurs.
	 */
	public JLabel getError() {
		return error;
	}
	/**
	 * Change le texte du JLabel de tour.
	 * @param txt Texte � mettre dans le JLabel.
	 */
	public void setTurnLbl(String txt) {
		this.turnLbl.setText(txt);
	}
	/**
	 * Gestion des clics sur les diff�rents boutons.
	 */
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "menu" :
			try {
				if((ss!=null)&&(!ss.isClosed())){
					ss.ecrirSurTousLesClients("deconnecte,;,1");
					ss.close();
				}
				if((cs!=null)&&(!cs.isClosed())){
					cs.ecrire("deconnecte,;,1");
					cs.close();
				}
			} catch (IOException e1) {
				System.out.println("Impossible de fermer le socket.");
			}
			mf.menu();
			break;
		case "passeTour" : 
			if(b.getTurn()){
				System.out.println("call passtour de multi");
				b.passTurn();
				b.send("passturn,;,1");
			}
			break;
		case "rules1" : 
			if(rulesLbl.isVisible()){
				rulesLbl.setVisible(false);
			}else{
				rulesLbl.setVisible(true);
			}
			break;
		default:
			break;
		}
	}

}

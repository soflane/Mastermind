/**
 * Package mettant en place toute l'interface graphique et le moteur du Mastermind.
 *
 *
 * @author R�my Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 *
 */
package be.ephec.mastermind.jeu;
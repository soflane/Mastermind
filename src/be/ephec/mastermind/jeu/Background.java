package be.ephec.mastermind.jeu;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * Classe permettant de cr�er un Background de type JLabel.
 * 
 * @author R�my Lambinet, Sofiane Ayoute et Nathan Meyer
 * @version 1.0
 */
@SuppressWarnings("serial")
public class Background extends JLabel{
	/**
	 * Constructeur du background.
	 */
	public Background(){
		this.setIcon(new ImageIcon(getClass().getResource("/background.gif")));
		this.setBounds(0, 0, Param.FRAMEWIDTH, Param.FRAMEHEIGHT);
	}
}